<?php


namespace App\Controller;
use App\Service\ApiController;
use Symfony\Component\HttpClient\HttpClient;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class BolApiController extends AbstractController
{
    /**
     * @var ApiController
     */
    private $apiController;
    /**
     * @var Response
     */
    private $response;

    public function __construct(ApiController $apiController)
    {

        $this->apiController = $apiController;
    }

    /**
     * @Route("/products/{searchString}", name="bol_api_search", methods={"GET"})
     * @param $searchString
     * @param $bol_api_key
     * @return JsonResponse
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
   public function getProducts($searchString, $bol_api_key){
       $this->apiController->initApi("GET");
       $client = HttpClient::create();
       $response = $client->request('GET', 'https://api.bol.com/catalog/v4/search/?q='.$searchString.'&apikey='.$bol_api_key.'&format=json&limit=10&dataoutput=categories,products'
         );
// op cat id zoeken: &ids=8299
       //get content in array
       $content = $response->toArray();

       $products = $content["products"];
        $myDadArray = [];
       foreach ($products as $oneproduct){
           $myDadArray[] = array("title" =>$oneproduct["title"],
               "id" =>$oneproduct["id"],
               "imgUrl" => $oneproduct["images"][2]["url"],
               "type" => $oneproduct["gpc"],
               "price" => $oneproduct["offerData"]["offers"][0]["price"],
               "basket" => false

           );
       }
//       dd($myDadArray);
       $returndata["products"] = $myDadArray;
       $returndata["category"] = $content["categories"];


     return new JsonResponse($returndata);

   }

    /**
     * @Route("/products/{searchString}/{category}", name="bol_api_cat_search", methods={"GET"})
     * @param $category
     * @param $bol_api_key
     * @return JsonResponse
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */

   public function getProductsWithCategory($category,$searchString, $bol_api_key){
       $this->apiController->initApi("GET");
       $client = HttpClient::create();
       $response = $client->request('GET', 'https://api.bol.com/catalog/v4/search/?q='.$searchString.'&apikey='.$bol_api_key.'&format=json&limit=10&dataoutput=categories,products&ids='.$category
       );

       $content = $response->toArray();


       $products = $content["products"];
       $myDadArray = [];
       foreach ($products as $oneproduct){
           $myDadArray[] = array("title" =>$oneproduct["title"],
               "id" =>$oneproduct["id"],
               "imgUrl" => $oneproduct["images"][2]["url"],
               "type" => $oneproduct["gpc"],
               "price" => $oneproduct["offerData"]["offers"][0]["price"]?$oneproduct["offerData"]["offers"][0]["price"]:"not-availeble",
               "basket" => false

           );
       }
//       dd($myDadArray);
       $returndata["products"] = $myDadArray;


       return new JsonResponse($returndata);

   }

    /**
     * @Route("/basketUpdate/{prodId}", name="bol_api_basket_insert", methods={"GET"})
     * @param $prodId
     * @param $bol_api_key
     * @return JsonResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function basketUpdate($prodId, $bol_api_key){
        $this->apiController->initApi("GET");
        $client = HttpClient::create();
        $response = $client->request('GET', 'https://api.bol.com/catalog/v4/products/'.$prodId.'?apikey='.$bol_api_key.'&offers=cheapest&includeAttributes=false&format=json'
        );

        $content = $response->toArray();


        $products = $content["products"];
        $myDadArray = [];
        foreach ($products as $oneproduct){
            $myDadArray[] = array("title" =>$oneproduct["title"],
                "id" =>$oneproduct["id"],
                "imgUrl" => $oneproduct["images"][2]["url"],
                "type" => $oneproduct["gpc"],
                "price" => $oneproduct["offerData"]["offers"][0]["price"]?$oneproduct["offerData"]["offers"][0]["price"]:"not-availeble",
                "total" => 1,
                "unitPrice" =>$oneproduct["offerData"]["offers"][0]["price"]?$oneproduct["offerData"]["offers"][0]["price"]:"not-availeble"

            );
        }
//       dd($myDadArray);
        $returndata["products"] = $myDadArray;


        return new JsonResponse($returndata);

    }


}